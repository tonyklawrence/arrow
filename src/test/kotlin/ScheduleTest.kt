import arrow.fx.coroutines.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import org.junit.jupiter.api.Test
import java.lang.Thread.sleep
import java.time.Clock
import kotlin.random.Random
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
class ScheduleTest {

    suspend fun doit(s: String): String {
        val timeNow = Clock.systemDefaultZone().instant().toString()
        println("$s $timeNow")
        return timeNow
    }

    suspend fun threadName() = Thread.currentThread().name

    @Test fun `pretend service`() = runBlocking {
        val response = Atomic(emptyList<String>())

        val forever = Schedule.recurs<Throwable>(10).delay { seconds(1) }
        val query = Schedule.forever<List<String>>().delay { seconds(1) }

        val writer = async {
            withContext(IO) {
                forever.retryOrElse({
                    query.repeat {
                        val bytes = Random.nextBytes(4).toList()
                        if (bytes[0] > 100) {
                            println("Error occurred in ${threadName()}")
                            throw java.lang.RuntimeException("F: ${bytes[0]} ${threadName()}")
                        }
                        response.set(bytes.map { "$it" })
                        println("W: ${response.get()} ${threadName()}")
                        response.get()
                    }
                }, { _, _ ->
                    emptyList<String>()
                })
            }
        }

        Schedule.recurs<Unit>(100).delay { milliseconds(500) }.repeat {
            println("R: ${response.get()} ${threadName()}")
        }

//        repeat((1..100).count()) {
//            println("R: ${response.get()} ${threadName()}")
//            sleep(500)
//        }

        writer.await()
        println("D: ${response.get() }")
    }

    @Test fun `test schedule`() = runBlocking {

        val forever = Schedule.forever<String>()
        val tenTimes: Schedule<String, Int> = Schedule.recurs<String>(10)
        val second = Schedule.spaced<String>(seconds(1))

        val everySecond = forever and second
        val tenSeconds = tenTimes and second

        val r = async {
            Schedule.recurs<Throwable>(6).delay { seconds(2) }.retryOrElseEither({
                tenSeconds.repeat {
                    doit("retry")
                    throw java.lang.RuntimeException("fail")
                }
            }) { _, _ ->
                doit("retry on error")
            }
        }

        val fibre = async {
            val result = tenSeconds.repeat {
                doit("aync")
            }

            println(result)
//            r.cancel()
        }

        println("Here: $fibre")
        fibre.await()
        r.await()
        println("Done: $fibre $r")
    }

    @Test fun `flow test`() = runBlocking{
        var counter = 0
        val flow: Flow<String> = flow {
            emit("a")
            if (++counter <= 5) throw RuntimeException("Bang!")
        }
        //sampleStart
        val sum = flow.retry(Schedule.recurs(5)).reduce { acc, int -> acc + int }
        //sampleEnd
        println(sum)
    }
}