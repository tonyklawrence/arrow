import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.31"
}

repositories { mavenCentral() }

dependencies {
    implementation("io.arrow-kt:arrow-core:1.0.1")
    implementation("io.arrow-kt:arrow-fx-coroutines:1.0.1")

    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("com.natpryce:hamkrest:1.8.0.1")
}

tasks {
    compileJava { sourceCompatibility = "16" }
    test { useJUnitPlatform() }

    withType<KotlinCompile>().configureEach { kotlinOptions.jvmTarget = "16" }
}